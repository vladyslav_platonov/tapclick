### tapclick
Application for test task from TapMedia.

---
- [Requirements](#requirements)
- [How to install virtualbox and vagrant](#how-to-install-virtualbox-and-vagrant)
- [Setup environment](#setup-environment)
- [Tests](#tests)
- [MySQL](#mysql)
- [Authors](#authors)


---

###### Structure


* [**src**](src) - Project work directory.
* [**root project directory**]()
    * [**Homestead.yaml**](Homestead.yaml) - Homestead configuration.
    * [**Vagrantfile**](Vagrantfile) - Project provision configuration.

---

###### Requirements

  * virtualbox >= 5.1.28
  * vagrant >= 2.0.0

---

###### Tested on

* OSX
* Ubuntu 16.04

---

# How to install virtualbox, vagrant and homestead

1. Download  and install the proper package of [vagrant](https://www.vagrantup.com/downloads.html) for your operating system and architecture.
2. Download and install [VirtualBox ](https://www.virtualbox.org/wiki/Downloads).
3. Add Homestead box to your vagrant, using command `vagrant box add laravel/homestead` .
> If you are using Windows, you may need to enable hardware virtualization (VT-x). It can usually be enabled via your BIOS. If you are using Hyper-V on a UEFI system you may additionally need to disable Hyper-V in order to access VT-x.


---

# Setup environment

1. Clone repository with submodules.
    ```
    git clone git@bitbucket.org:vladyslav_platonov/tapclick.git
    ```
2. Install project dependencies using composer.
    ```
    composer install
    ```
3. Go to project folder
    ```
    cd {project_dir}
    ```
4. Run homestead
    ```
    vagrant up
    ```
5. Connect to homestead machine 
    ```
    vagrant ssh
    ```
6. Go to project folder
    ```
    cd ~/code
    ```
7. Clear symfony cache
    ```
    bin/console cache:clear
    ```
8. Update database schema
    ```
    bin/console doctrine:schema:update -f -n
    ```
9. Load test data with fixtures
    ```
    bin/console doctrine:fixtures:load -n
    ```
10. Open environment "http://tapclick.local/"

---

# Tests
For current project there are both - unit and functional tests. 
Coverage statistic:
* **38** tests
* **135** assertions
* **100%** test coverage

###To run project tests, please run following commands:
1. Go to project folder
    ```
    cd {project_dir}
    ```
2. Run homestead if it's not running yet
    ```
    vagrant up
    ```
3. Connect to homestead machine 
    ```
    vagrant ssh
    ```
4. Go to project folder
    ```
    cd ~/code
    ```
5. Load test data with fixtures
    ```
    bin/console doctrine:fixtures:load -n
    ```
6. Run phpunit
    ```
    bin/phpunit
    ```

---

# MySQL
* MySQL creadentials: homestead / secret

---

# Authors

Vladyslav Platonov (vladyslav.platonov@gmail.com)

---
