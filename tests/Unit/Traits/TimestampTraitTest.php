<?php

namespace App\Tests\Unit\Traits;

use App\Traits\TimestampTrait;
use DateTime;
use PHPUnit\Framework\TestCase;


/**
 * Class TimestampTraitTest
 */
class TimestampTraitTest extends TestCase
{
    use TimestampTrait;

    /**
     * Test timestamp trait
     */
    public function testTrait()
    {
        $this->assertNull($this->getCreatedAt());
        $this->assertNull($this->getUpdatedAt());

        $this->setCreatedAt(new DateTime('2018-03-18 12:00:00'));
        $this->setUpdatedAt(new DateTime('2018-03-18 13:00:00'));

        $this->assertEquals(new DateTime('2018-03-18 12:00:00'), $this->getCreatedAt());
        $this->assertEquals(new DateTime('2018-03-18 13:00:00'), $this->getUpdatedAt());

        $this->updateTimestamps();
        $this->assertInstanceOf(DateTime::class, $this->getCreatedAt());
        $this->assertInstanceOf(DateTime::class, $this->getUpdatedAt());
    }
}