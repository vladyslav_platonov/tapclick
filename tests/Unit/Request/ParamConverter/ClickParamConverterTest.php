<?php

namespace App\Tests\Unit\Request\ParamConverter;

use App\Entity\Click;
use App\Request\ParamConverter\ClickParamConverter;
use App\Service\ClickCreator;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\Common\Persistence\Mapping\ClassMetadata;
use Doctrine\Common\Persistence\ObjectManager;
use InvalidArgumentException;
use PHPUnit\Framework\TestCase;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\Request;


/**
 * Class ClickParamConverterTest
 */
class ClickParamConverterTest extends TestCase
{
    /**
     * @var ManagerRegistry
     */
    private $registry;

    /**
     * @var ClickParamConverter
     */
    private $converter;

    /**
     * @var ClickCreator
     */
    private $clickCreator;


    /**
     * Setup data for all cases
     */
    protected function setUP()
    {
        $this->registry = $this->getMockBuilder(ManagerRegistry::class)->getMock();
        $this->clickCreator = $this->getMockBuilder(ClickCreator::class)
            ->disableOriginalConstructor()
            ->getMock();
        $this->converter = new ClickParamConverter($this->registry, $this->clickCreator);
    }

    protected function createConfiguration($class = null, $name = 'arg')
    {
        $methods = ['getClass', 'getAliasName', 'getOptions', 'getName', 'allowArray'];
        $config = $this
            ->getMockBuilder(ParamConverter::class)
            ->setMethods($methods)
            ->disableOriginalConstructor()
            ->getMock();
        $config->expects($this->any())
            ->method('getClass')
            ->will($this->returnValue($class));
        $config->expects($this->any())
            ->method('getName')
            ->will($this->returnValue($name));

        return $config;
    }

    /**
     * @param array $query
     * @param array $server
     * @return Request
     */
    protected function createRequest($query = [], $server = [])
    {
        $request = new Request();
        foreach ($query as $key => $value) {
            $request->query->set($key, $value);
        }
        foreach ($server as $key => $value) {
            $request->server->set($key, $value);
        }

        return $request;
    }

    /**
     * Test with good request
     * @throws \Doctrine\ORM\NonUniqueResultException'
     */
    public function testApplyGoodRequest()
    {
        $params = [
            'referer' => 'http://google.com.ua/search/test?q=test',
            'ip' => '192.168.10.10',
            'userAgent' => 'test agent string 1',
            'param1' => 'value11',
            'param2' => 'value22',
        ];

        $requestMock = $this->createRequest(
            ['param1' => $params['param1'], 'param2' => $params['param2']],
            ['HTTP_REFERER' => $params['referer'], 'REMOTE_ADDR' => $params['ip'], 'HTTP_USER_AGENT' => $params['userAgent']]
        );

        $this->clickCreator->expects($this->any())
            ->method('createClick')
            ->will($this->returnValue(new Click(
                $params['referer'],
                $params['ip'],
                $params['userAgent'],
                $params['param1'],
                $params['param2']
            )));

        $config = $this->createConfiguration(Click::class);

        $this->converter->apply($requestMock, $config);

        $this->assertInstanceOf(Click::class, $requestMock->attributes->get($config->getName()));
    }

    /**
     * Test with bad request
     * @throws \Doctrine\ORM\NonUniqueResultException'
     */
    public function testApplyBadRequest()
    {
        $requestMock = $this->createRequest();

        $config = $this->createConfiguration(Click::class);

        $this->expectException(InvalidArgumentException::class);
        $this->converter->apply($requestMock, $config);
    }

    /**
     * Test supports method
     */
    public function testSupports()
    {
        $config = $this->createConfiguration(Click::class, []);
        $classMetadata = $this->getMockBuilder(ClassMetadata::class)->getMock();

        $objectManager = $this->getMockBuilder(ObjectManager::class)->getMock();

        $classMetadata->expects($this->once())
            ->method('getName')
            ->will($this->returnValue(Click::class));

        $objectManager->expects($this->any())
            ->method('getClassMetadata')
            ->will($this->returnValue($classMetadata));

        $this->registry->expects($this->any())
            ->method('getManagers')
            ->will($this->returnValue([$objectManager]));

        $this->registry->expects($this->once())
            ->method('getManagerForClass')
            ->with(Click::class)
            ->will($this->returnValue($objectManager));

        $result = $this->converter->supports($config);

        $this->assertTrue($result);
    }

    /**
     * Test supports method without registry managers
     */
    public function testSupportsWithDifferentConfigClass()
    {
        $config = $this->createConfiguration('stdObject');

        $classMetadata = $this->getMockBuilder(ClassMetadata::class)->getMock();

        $objectManager = $this->getMockBuilder(ObjectManager::class)->getMock();

        $classMetadata->expects($this->once())
            ->method('getName')
            ->will($this->returnValue('stdObject'));

        $objectManager->expects($this->any())
            ->method('getClassMetadata')
            ->will($this->returnValue($classMetadata));

        $this->registry->expects($this->any())
            ->method('getManagers')
            ->will($this->returnValue([$objectManager]));

        $this->registry->expects($this->once())
            ->method('getManagerForClass')
            ->with('stdObject')
            ->will($this->returnValue($objectManager));

        $result = $this->converter->supports($config);

        $this->assertFalse($result);
    }

    /**
     * Test supports method without registry managers
     */
    public function testSupportsWithoutRegistryManagers()
    {
        $config = $this->createConfiguration(Click::class);

        $this->registry->expects($this->any())
            ->method('getManagers')
            ->will($this->returnValue([]));

        $result = $this->converter->supports($config);

        $this->assertFalse($result);
    }

    /**
     * Test supports method without registry managers
     */
    public function testSupportsWithoutConfigClass()
    {
        $config = $this->createConfiguration(null);

        $objectManager = $this->getMockBuilder(ObjectManager::class)->getMock();

        $this->registry->expects($this->any())
            ->method('getManagers')
            ->will($this->returnValue([$objectManager]));

        $result = $this->converter->supports($config);

        $this->assertFalse($result);
    }
}