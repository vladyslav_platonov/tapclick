<?php

namespace App\Tests\Unit\Converter;

use App\Converter\UrlToDomainConverter;
use PHPUnit\Framework\TestCase;


/**
 * Class UrlToDomainConverterTest
 */
class UrlToDomainConverterTest extends TestCase
{
    public function testApply()
    {
        $pattern = '/^(?:https?:\/\/)?(?:w{3})?\.?([\w\d\.-]+\.[\w]{2,5}).*$/';
        $converter = new UrlToDomainConverter($pattern);

        // Check http, https, without protocol, with and without www, with and without route parameters
        $urlList = [
            'google.com',
            'www.google.com',
            'http://google.com/',
            'http://www.google.com/',
            'https://google.com/',
            'https://www.google.com/',
            'http://google.com/test/',
            'http://www.google.com/test/',
            'https://google.com/test/',
            'https://www.google.com/test/',
            'http://google.com/test/?param1=test&param2=test',
            'http://www.google.com/test/?param1=test&param2=test',
            'https://google.com/test/?param1=test&param2=test',
            'https://www.google.com/test/?param1=test&param2=test',
        ];

        foreach ($urlList as $url) {
            $this->assertEquals('google.com', $converter->apply($url));
        }

        // Check with hyphen in domain
        $urlList = [
            'google-test.com.ua',
            'www.google-test.com.ua',
            'http://google-test.com.ua/',
            'http://www.google-test.com.ua/',
            'https://google-test.com.ua/',
            'https://www.google-test.com.ua/',
            'http://google-test.com.ua/test/',
            'http://www.google-test.com.ua/test/',
            'https://google-test.com.ua/test/',
            'https://www.google-test.com.ua/test/',
            'http://google-test.com.ua/test/?param1=test&param2=test',
            'http://www.google-test.com.ua/test/?param1=test&param2=test',
            'https://google-test.com.ua/test/?param1=test&param2=test',
            'https://www.google-test.com.ua/test/?param1=test&param2=test',
        ];

        foreach ($urlList as $url) {
            $this->assertEquals('google-test.com.ua', $converter->apply($url));
        }

        // Check with whitespaces
        $urlList = [
            ' google.com',
            'www.google.com ',
            ' http://google.com/',
            'http://www.google.com/  ',
            '  https://google.com/',
            ' https://www.google.com/ ',
            '  http://google.com/test/  ',
        ];

        foreach ($urlList as $url) {
            $this->assertEquals('google.com', $converter->apply($url));
        }

        // Check with not valid url
        $this->expectException(\InvalidArgumentException::class);
        $converter->apply('test@gmail.com');
    }

    public function testFormatNotValidException()
    {
        $this->expectExceptionMessage('Format is not valid regular expression');
        new UrlToDomainConverter('test');
    }

    public function testFormatEmptyException()
    {
        $this->expectExceptionMessage('Format is not valid regular expression');
        new UrlToDomainConverter('');
    }
}