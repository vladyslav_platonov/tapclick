<?php

namespace App\Tests\Unit\Service;

use App\Entity\Click;
use App\Repository\ClickRepository;
use App\Service\BadDomainChecker;
use App\Service\ClickCreator;
use PHPUnit\Framework\TestCase;
use ReflectionClass;
use Symfony\Component\Validator\Validator\ValidatorInterface;


/**
 * Class ClickCreatorTest
 */
class ClickCreatorTest extends TestCase
{
    /**
     * Test create new click
     *
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function testCreateClick()
    {
        $repositoryMock = $this->getMockBuilder(ClickRepository::class)
            ->disableOriginalConstructor()
            ->getMock();

        $repositoryMock->expects($this->any())
            ->method('findOneByData')
            ->willReturn(null);

        $badDomainCheckerMock = $this->getMockBuilder(BadDomainChecker::class)
            ->disableOriginalConstructor()
            ->getMock();

        $validatorMock = $this->getMockBuilder(ValidatorInterface::class)
            ->disableOriginalConstructor()
            ->getMock();

        $clickCreator = new ClickCreator($repositoryMock, $badDomainCheckerMock, $validatorMock);

        $params = [
            'referer' => 'http://google.com.ua/search/test?q=test',
            'ip' => '192.168.10.10',
            'userAgent' => 'test agent string 1',
            'param1' => 'value11',
            'param2' => 'value22',
        ];

        $click = $clickCreator->createClick(
            $params['referer'],
            $params['ip'],
            $params['userAgent'],
            $params['param1'],
            $params['param2']
        );

        $this->assertEquals($params['referer'], $click->getReferer());
        $this->assertEquals($params['ip'], $click->getIp());
        $this->assertEquals($params['userAgent'], $click->getUserAgent());
        $this->assertEquals($params['param1'], $click->getParam1());
        $this->assertEquals($params['param2'], $click->getParam2());
    }

    /**
     * Test get click from storage
     *
     * @throws \Doctrine\ORM\NonUniqueResultException
     * @throws \ReflectionException
     */
    public function testGetClickFromStorage()
    {

        $repositoryMock = $this->getMockBuilder(ClickRepository::class)
            ->disableOriginalConstructor()
            ->getMock();

        $params = [
            'id' => 'cbcf736d-6d59-44f5-bcc6-ca118b978383',
            'referer' => 'http://google.com.ua/search/test?q=test',
            'ip' => '192.168.10.10',
            'userAgent' => 'test agent string 1',
            'param1' => 'value11',
            'param2' => 'value22',
        ];

        $storageClick = new Click(
            $params['referer'],
            $params['ip'],
            $params['userAgent'],
            $params['param1'],
            $params['param2']
        );

        $reflection = new ReflectionClass($storageClick);
        $property = $reflection->getProperty('id');
        $property->setAccessible(true);
        $property->setValue($storageClick, $params['id']);

        $repositoryMock->expects($this->any())
            ->method('findOneByData')
            ->willReturn($storageClick);

        $badDomainCheckerMock = $this->getMockBuilder(BadDomainChecker::class)
            ->disableOriginalConstructor()
            ->getMock();

        $validatorMock = $this->getMockBuilder(ValidatorInterface::class)
            ->disableOriginalConstructor()
            ->getMock();

        $clickCreator = new ClickCreator($repositoryMock, $badDomainCheckerMock, $validatorMock);


        $click = $clickCreator->createClick(
            $params['referer'],
            $params['ip'],
            $params['userAgent'],
            $params['param1'],
            $params['param2']
        );

        $this->assertEquals($params['id'], $click->getId());
        $this->assertEquals($params['referer'], $click->getReferer());
        $this->assertEquals($params['ip'], $click->getIp());
        $this->assertEquals($params['userAgent'], $click->getUserAgent());
        $this->assertEquals($params['param1'], $click->getParam1());
        $this->assertEquals($params['param2'], $click->getParam2());
    }
}