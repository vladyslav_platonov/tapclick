<?php

namespace App\Tests\Unit\Service;

use App\Converter\UrlToDomainConverter;
use App\Repository\BadDomainRepository;
use App\Service\BadDomainChecker;
use InvalidArgumentException;
use PHPUnit\Framework\TestCase;
use stdClass;


/**
 * Class BadDomainCheckerTest
 */
class BadDomainCheckerTest extends TestCase
{
    /**
     * @var BadDomainRepository
     */
    private $badDomainRepositoryMock;

    /**
     * @var UrlToDomainConverter
     */
    private $urlToDomain;

    /**
     * Setup data for all cases
     */
    protected function setUP()
    {
        $this->badDomainRepositoryMock = $this->getMockBuilder(BadDomainRepository::class)
            ->disableOriginalConstructor()
            ->getMock();


        $pattern = '/^(?:https?:\/\/)?(?:w{3})?\.?([\w\d\.-]+\.[\w]{2,5}).*$/';
        $this->urlToDomain = new UrlToDomainConverter($pattern);
    }

    /**
     * Test if isBadUrlDomain function works good without bad domain
     */
    public function testIsBadUrlDomainWithoutBadDomain()
    {
        $this->badDomainRepositoryMock->expects($this->any())
            ->method('findOneByName')
            ->willReturn(null);

        $badDomainChecker = new BadDomainChecker($this->badDomainRepositoryMock, $this->urlToDomain);

        $this->assertFalse($badDomainChecker->isBadUrlDomain('http://www.google.com/'));
        $this->assertFalse($badDomainChecker->isBadUrlDomain('google.com'));
        $this->expectException(InvalidArgumentException::class);
        $this->assertFalse($badDomainChecker->isBadUrlDomain('test@google.com'));
    }

    /**
     * Test if isBadUrlDomain function works good with bad domain
     */
    public function testIsBadUrlDomainWithBadDomain()
    {
        $stdDomainObject = new stdClass();

        $this->badDomainRepositoryMock->expects($this->any())
            ->method('findOneByName')
            ->willReturn($stdDomainObject);

        $badDomainChecker = new BadDomainChecker($this->badDomainRepositoryMock, $this->urlToDomain);

        $this->assertTrue($badDomainChecker->isBadUrlDomain('http://www.google.com/'));
        $this->assertTrue($badDomainChecker->isBadUrlDomain('google.com'));
        $this->expectException(InvalidArgumentException::class);
        $this->assertFalse($badDomainChecker->isBadUrlDomain('test@google.com'));
    }
}