<?php

namespace App\Tests\Unit\Entity;

use App\Entity\BadDomain;
use PHPUnit\Framework\TestCase;


/**
 * Class BadDomainTest
 */
class BadDomainTest extends TestCase
{
    /**
     * Test entity data with changing name
     */
    public function testEntityData()
    {
        $domain = new BadDomain();

        $this->assertNull($domain->getId());
        $this->assertNull($domain->getName());
        $this->assertNull($domain->getCreatedAt());
        $this->assertNull($domain->getUpdatedAt());

        $domain->setName('test.test');
        $this->assertEquals('test.test', $domain->getName());
    }
}