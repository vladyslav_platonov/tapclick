<?php

namespace App\Tests\Unit\Entity;

use App\Entity\Click;
use App\Service\BadDomainChecker;
use PHPUnit\Framework\TestCase;
use ReflectionClass;
use Symfony\Component\Validator\Exception\ValidatorException;
use Symfony\Component\Validator\Validator\ValidatorInterface;


/**
 * Class ClickTest
 */
class ClickTest extends TestCase
{
    /**
     * @var Click
     */
    private $click;

    /**
     * @var array
     */
    private $params = [
        'referer' => 'http://google.com/search/test?q=test',
        'ip' => '192.168.0.10',
        'userAgent' => 'test agent string',
        'param1' => 'value1',
        'param2' => 'value2',
    ];

    /**
     * {@inheritdoc}
     */
    protected function setUP()
    {
        $this->click = new Click(
            $this->params['referer'],
            $this->params['ip'],
            $this->params['userAgent'],
            $this->params['param1'],
            $this->params['param2']
        );
    }

    /**
     * Test Click constructor to put correct data for class
     */
    public function testConstructor()
    {
        $this->assertNull($this->click->getId());
        $this->assertEquals($this->params['referer'], $this->click->getReferer());
        $this->assertEquals($this->params['ip'], $this->click->getIp());
        $this->assertEquals($this->params['userAgent'], $this->click->getUserAgent());
        $this->assertEquals($this->params['param1'], $this->click->getParam1());
        $this->assertEquals($this->params['param2'], $this->click->getParam2());
        $this->assertEquals(0, $this->click->getErrorCount());
        $this->assertFalse($this->click->isBadDomain());
        $this->assertNull($this->click->getCreatedAt());
        $this->assertNull($this->click->getUpdatedAt());
    }

    /**
     * Test if handleClick method returns true without domain check
     */
    public function testSuccessHandleClickWithoutDomainCheck()
    {
        $this->assertTrue($this->click->handleClick());
        $this->assertFalse($this->click->isBadDomain());
        $this->assertEquals(0, $this->click->getErrorCount());
    }

    /**
     * Test if handleClick method returns false without domain check with id
     * @throws \ReflectionException
     */
    public function testFailureHandleClickWithoutDomainCheck()
    {
        $reflection = new ReflectionClass($this->click);
        $property = $reflection->getProperty('id');
        $property->setAccessible(true);
        $property->setValue($this->click, 'cbcf736d-6d59-44f5-bcc6-ca118b978383');

        $this->assertFalse($this->click->handleClick());
        $this->assertFalse($this->click->isBadDomain());
        $this->assertEquals(1, $this->click->getErrorCount());
    }

    /**
     * Test if handleClick method returns true with domain check
     */
    public function testSuccessHandleClickWithDomainCheck()
    {
        $badDomainCheckerMock = $this->getMockBuilder(BadDomainChecker::class)
            ->disableOriginalConstructor()
            ->getMock();

        $badDomainCheckerMock->expects($this->once())
            ->method('isBadUrlDomain')
            ->willReturn(false);

        $this->click->setBadDomainChecker($badDomainCheckerMock);
        $this->assertTrue($this->click->handleClick());
        $this->assertFalse($this->click->isBadDomain());
        $this->assertEquals(0, $this->click->getErrorCount());
    }

    /**
     * Test if handleClick method returns false with domain check
     */
    public function testFailureHandleClickWithDomainCheck()
    {
        $badDomainCheckerMock = $this->getMockBuilder(BadDomainChecker::class)
            ->disableOriginalConstructor()
            ->getMock();
        $badDomainCheckerMock->expects($this->once())
            ->method('isBadUrlDomain')
            ->willReturn(true);
        $this->click->setBadDomainChecker($badDomainCheckerMock);

        $this->assertFalse($this->click->handleClick());
        $this->assertTrue($this->click->isBadDomain());
        $this->assertEquals(1, $this->click->getErrorCount());
    }

    /**
     * Test data validation success
     */
    public function testValidationSuccess()
    {
        $validator = $this->getMockBuilder(ValidatorInterface::class)
            ->disableOriginalConstructor()
            ->getMock();

        $validator->expects($this->once())
            ->method('validate')
            ->will($this->returnValue([]));
        $this->click->setValidator($validator);

        $this->assertTrue($this->click->handleClick());
    }

    /**
     * Test data validation failure
     */
    public function testValidationFailure()
    {
        $validator = $this->getMockBuilder(ValidatorInterface::class)
            ->disableOriginalConstructor()
            ->getMock();

        $validator->expects($this->once())
            ->method('validate')
            ->will($this->throwException(new ValidatorException()));
        $this->click->setValidator($validator);

        $this->expectException(ValidatorException::class);
        $this->click->handleClick();
    }
}