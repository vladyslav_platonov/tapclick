<?php

namespace App\Tests\Unit\Repository;

use App\Entity\Click;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;


/**
 * Test repository for Click entities
 */
class ClickRepositoryTest extends WebTestCase
{
    /**
     * @var \App\Repository\ClickRepository
     */
    private $repository;

    /**
     * @var array Test parameters
     */
    private $params = [
        'referer' => 'http://google.com.ua/search/test?q=test',
        'ip' => '192.168.10.10',
        'userAgent' => 'test agent string 1',
        'param1' => 'value11',
        'param2' => 'value22',
    ];

    /**
     * Setup data for all cases
     */
    protected function setUP()
    {
        $kernel = static::createKernel();
        $kernel->boot();
        $this->repository = $kernel->getContainer()
            ->get('doctrine.orm.entity_manager')
            ->getRepository(Click::class);
    }

    /**
     * Test if findAll method returns good count of entities
     */
    public function testFindAll()
    {
        $result = $this->repository->findAll(2);
        $this->assertEquals(2, count($result));

        $result = $this->repository->findAll(5);
        $this->assertEquals(5, count($result));
        $this->assertInstanceOf(Click::class, $result[0]);
    }

    /**
     * Test click saving
     *
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function testSave()
    {
        $click = new Click(
            $this->params['referer'],
            $this->params['ip'],
            $this->params['userAgent'],
            $this->params['param1'],
            $this->params['param2']
        );

        $this->assertTrue($this->repository->save($click));
    }

    /**
     * Test find click with good data
     *
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function testFindOneByGoodData()
    {
        $result = $this->repository->findOneByData(
            $this->params['referer'],
            $this->params['ip'],
            $this->params['userAgent'],
            $this->params['param1']
        );

        $this->assertInstanceOf(Click::class, $result);
    }

    /**
     * Test find click with wrong data
     *
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function testFindOneByBadData()
    {
        $result = $this->repository->findOneByData(
            $this->params['referer'] . '_test',
            $this->params['ip'],
            $this->params['userAgent'],
            $this->params['param1']
        );

        $this->assertNull($result);
    }
}