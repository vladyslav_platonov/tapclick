<?php

namespace App\Tests\Unit\Repository;

use App\Entity\BadDomain;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;


/**
 * Test repository for BadDomain entities
 */
class BadDomainRepositoryTest extends WebTestCase
{
    /**
     * @var \App\Repository\BadDomainRepository
     */
    private $repository;

    /**
     * @var string Test parameters
     */
    private $testName = 'google.de';

    /**
     * Setup data for all cases
     */
    protected function setUP()
    {
        $kernel = static::createKernel();
        $kernel->boot();
        $this->repository = $kernel->getContainer()
            ->get('doctrine.orm.entity_manager')
            ->getRepository(BadDomain::class);
    }

    /**
     * Test if findAll method returns good count of entities
     */
    public function testFindAll()
    {
        $result = $this->repository->findAll(2);
        $this->assertEquals(2, count($result));

        $result = $this->repository->findAll(5);
        $this->assertEquals(5, count($result));
        $this->assertInstanceOf(BadDomain::class, $result[0]);
    }

    /**
     * Test domain saving
     *
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function testSave()
    {
        $domain = new BadDomain();
        $domain->setName($this->testName);

        $this->assertTrue($this->repository->save($domain));
    }

    /**
     * Test find domain with good data
     *
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function testFindOneByGoodData()
    {
        $domain = $this->repository->findOneByName($this->testName);

        $this->assertInstanceOf(BadDomain::class, $domain);
    }

    /**
     * Test find domain with wrong data
     *
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function testFindOneByBadData()
    {
        $result = $this->repository->findOneByName('testtesttest');

        $this->assertNull($result);
    }

    /**
     * Test removing domain from storage
     *
     * @throws \Doctrine\ORM\NonUniqueResultException
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function testRemove()
    {
        $domain = $this->repository->findOneByName($this->testName);
        $this->assertInstanceOf(BadDomain::class, $domain);

        $this->assertTrue($this->repository->remove($domain));
    }
}