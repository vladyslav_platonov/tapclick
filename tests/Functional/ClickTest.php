<?php

namespace App\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Client;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\DomCrawler\Crawler;


/**
 * Functional Test for clicks
 */
class ClickTest extends WebTestCase
{
    /**
     * Test click list
     */
    public function testClickList()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/');

        // Check there are some clicks on the page
        $this->assertTrue($crawler->filter('table#clickTable tbody tr')->count() > 0);
    }

    /**
     * Test add click
     */
    public function testAddClick()
    {
        $client = static::createClient();
        $crawler = $this->addOrUpdateClick($client);

        // Check if we have information about click on the page
        $this->assertTrue($crawler->filter('ul.click-data li')->count() > 0);

        $this->assertEquals('Click success', $crawler->filter('head title')->text());
        $this->assertEquals('Error count: 0', $crawler->filter('ul.click-data li.click-error')->text());
    }

    /**
     * Test update click
     */
    public function testUpdateClick()
    {
        $client = static::createClient();
        $crawler = $this->addOrUpdateClick($client);

        $this->assertEquals('Click error', $crawler->filter('head title')->text());

        // Check if we have error message about bad domain
        $this->assertEquals(
            'Click data already exist.',
            trim($crawler->filter('div.alert-danger')->text())
        );
    }

    public function testBadDomainClick()
    {
        $client = static::createClient();

        // First we should add domain to bad list
        $crawler = $client->request('GET', '/bad_domains/');
        $form = $crawler->selectButton('Add domain')->form();
        $client->submit($form, [
            'form[name]' => 'google.ua',
        ]);

        // Need to follow redirect
        $crawler = $client->followRedirect();

        // Check if domain is added to list
        $this->assertEquals('Domain added to list', $crawler->filter('form span.badge-success')->text());

        // Add click
        $client = static::createClient();
        $crawler = $this->addOrUpdateClick($client);
        $this->assertEquals('Click error', $crawler->filter('head title')->text());

        // Check if we have error message about bad domain
        $this->assertEquals(
            'Domain of click referer is bad one! You will be redirected after few seconds.',
            trim($crawler->filter('div.alert-danger')->text())
        );
    }

    /**
     * @param Client $client
     * @return \Symfony\Component\DomCrawler\Crawler
     */
    private function addOrUpdateClick(Client $client): Crawler
    {
        // Mock needed server data
        $serverData = ['HTTP_REFERER' => 'http://google.ua/', 'HTTP_USER_AGENT' => 'test agent 1', 'REMOTE_ADDR' => '127.1.1.1'];

        // Open needed page
        $client->request('GET', '/click/?param1=test111&param2=test222', [], [], $serverData);

        // Check if response is redirection
        $this->assertTrue($client->getResponse()->isRedirection());

        // Need to follow redirect
        $crawler = $client->followRedirect();

        return $crawler;
    }
}