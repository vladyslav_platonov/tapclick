<?php

namespace App\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;


/**
 * Functional test for bad domains
 */
class BadDomainTest extends WebTestCase
{
    /**
     * Test click list
     */
    public function testBadDomainList()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/bad_domains/');

        // Check there are some domains on the page
        $this->assertTrue($crawler->filter('table#badDomainTable tbody tr')->count() > 0);
    }

    public function testBadDomainAdd()
    {
        $client = static::createClient();

        // First we should add domain to bad list
        $crawler = $client->request('GET', '/bad_domains/');
        $form = $crawler->selectButton('Add domain')->form();
        $client->submit($form, [
            'form[name]' => 'test.test',
        ]);

        // Need to follow redirect
        $crawler = $client->followRedirect();

        // Check if domain is added to list
        $this->assertEquals('Domain added to list', $crawler->filter('form span.badge-success')->text());

        // Check with the same domain for validation test
        $form = $crawler->selectButton('Add domain')->form();
        $client->submit($form, [
            'form[name]' => 'test.test',
        ]);

        $crawler = $client->followRedirect();
        $this->assertEquals(
            'This value is already used.',
            $crawler->filter('form div.invalid-feedback ul li span.form-error-message')->text()
        );
    }

    public function testBadDomainRemove()
    {
        $client = static::createClient();

        // First we should add domain to bad list
        $crawler = $client->request('GET', '/bad_domains/');

        $firstRowCrawler = $crawler->filter('table#badDomainTable tbody tr')->first();

        $badDomainId = $crawler->filter('td')->first()->text();

        $deleteUrl = $firstRowCrawler->selectLink('Delete')->link()->getUri();

        $client->request('GET', $deleteUrl);

        $this->assertTrue($client->getResponse()->isRedirection());

        $crawler = $client->followRedirect();

        $this->assertNotEquals(
            $badDomainId,
            $crawler->filter('table#badDomainTable tbody tr')->first()->filter('td')->first()->text()
        );
    }
}