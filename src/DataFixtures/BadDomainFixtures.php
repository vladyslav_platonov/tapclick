<?php

namespace App\DataFixtures;

use App\Entity\BadDomain;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Bundle\FixturesBundle\ORMFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Faker\Factory;


/**
 * Fixture to create test bad domains in storage
 *
 * @codeCoverageIgnore
 */
class BadDomainFixtures extends Fixture implements ORMFixtureInterface
{
    /**
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        $faker = Factory::create();

        for ($i = 0; $i < 10; $i++) {
            $domain = new BadDomain();
            $domain->setName($faker->unique()->domainName);
            $manager->persist($domain);
        }

        $manager->flush();
    }
}