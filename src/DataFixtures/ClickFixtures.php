<?php

namespace App\DataFixtures;

use App\Entity\Click;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Bundle\FixturesBundle\ORMFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Faker\Factory;


/**
 * Fixture to create test clicks in storage
 *
 * @codeCoverageIgnore
 */
class ClickFixtures extends Fixture implements ORMFixtureInterface
{
    /**
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        $faker = Factory::create();

        for ($i = 0; $i < 100; $i++) {
            $click = new Click($faker->url, $faker->unique()->ipv4, $faker->userAgent, $faker->unique()->word, $faker->word);
            $manager->persist($click);
        }

        $manager->flush();
    }
}