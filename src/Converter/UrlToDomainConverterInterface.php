<?php

namespace App\Converter;


interface UrlToDomainConverterInterface
{
    /**
     * Apply converter for given url
     *
     * @param string $url Url to apply
     *
     * @return string Domain name from given url
     *
     * @throws \InvalidArgumentException Thrown if url is not matched with pattern
     */
    public function apply(string $url);

    /**
     * @return string Url pattern from parameters
     */
    public function getPattern(): string;
}