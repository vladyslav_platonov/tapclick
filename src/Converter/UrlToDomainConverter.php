<?php

namespace App\Converter;


/**
 * Help class to convert domain name from given url
 */
class UrlToDomainConverter implements UrlToDomainConverterInterface
{
    /**
     * @var string Pattern to use
     */
    protected $pattern;

    /**
     * @param string $pattern Pattern for checking
     */
    public function __construct(string $pattern)
    {
        $this->pattern = $this->transformPattern($pattern);
    }

    /**
     * Apply converter for given url
     *
     * @param string $url Url to apply
     *
     * @return string Domain name from given url
     *
     * @throws \InvalidArgumentException Thrown if url is not matched with pattern
     */
    public function apply(string $url)
    {
        $url = $this->transformUrl($url);

        // If url is not matched with pattern, throw exception
        if (preg_match($this->getPattern(), $url, $matches) === false || count($matches) < 2) {
            throw new \InvalidArgumentException(sprintf(
                'Url "%s" is not valid, should be in next format: "%s"',
                $url,
                $this->getPattern()
            ));
        }

        return $matches[1];
    }

    /**
     * @return string Url pattern from parameters
     */
    public function getPattern(): string
    {
        return $this->pattern;
    }

    /**
     * Transforms pattern and checks whether string is valid pattern
     *
     * @param string $pattern Pattern to transform
     *
     * @return string Transformed pattern
     */
    private function transformPattern(string $pattern): string
    {
        $pattern = trim($pattern);

        if (!preg_match('/^\/[\s\S]+\/$/', $pattern)) {
            throw new \InvalidArgumentException('Format is not valid regular expression');
        }

        return $pattern;
    }

    /**
     * Transforms url
     *
     * @param string $url url to transform
     *
     * @return string Transformed url
     */
    private function transformUrl(string $url): string
    {
        return trim($url);
    }
}