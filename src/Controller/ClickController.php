<?php

namespace App\Controller;

use App\Entity\Click;
use App\Repository\ClickRepositoryInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;


/**
 * Controller for clicks
 */
class ClickController extends Controller
{
    /**
     * Add or update click
     *
     * @ParamConverter("click", class="App:Click", converter="click_converter")
     *
     * @param Click $click Click object from param converter
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function addOrUpdate(Click $click)
    {
        $result = $click->handleClick();
        $this->getRepository()->save($click);

        if ($result) {
            return $this->redirectToRoute('click_success', ['id' => $click->getId()]);
        } else {
            return $this->redirectToRoute('click_error', ['id' => $click->getId()]);
        }
    }

    /**
     * Success page for unique click
     *
     * @ParamConverter("click", class="App:Click", converter="doctrine.orm")
     *
     * @param Click $click
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function success(Click $click)
    {
        return $this->render('Click/success.html.twig', ['click' => $click]);
    }

    /**
     * Error page for not unique click or click with bad referer
     *
     * @ParamConverter("click", class="App:Click", converter="doctrine.orm")
     *
     * @param Click $click
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function error(Click $click)
    {
        $response = $this->render('Click/error.html.twig', ['click' => $click]);

        if ($click->isBadDomain()) {
            $redirectTimeout = $this->getParameter('click.error.redirect_timeout');
            $redirectUrl = $this->getParameter('click.error.redirect_url');

            $response->headers->set('Refresh', "{$redirectTimeout}; url={$redirectUrl}");
        }

        return $response;
    }

    /**
     * List of stored clicks
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function list()
    {
        $clicks = $this->getRepository()->findAll();

        return $this->render('Click/list.html.twig', ['clicks' => $clicks]);

    }

    /**
     * Get repository for Click entity
     *
     * @return \App\Repository\ClickRepositoryInterface Click repository
     */
    protected function getRepository(): ClickRepositoryInterface
    {
        return $this->getDoctrine()->getRepository(Click::class);
    }
}