<?php

namespace App\Controller;

use App\Entity\BadDomain;
use App\Repository\BadDomainRepositoryInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Session;


/**
 * Controller for bad domains
 */
class BadDomainController extends Controller
{
    /**
     * Add new domain
     *
     * @param Request $request
     * @param Session $session
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function add(Request $request, Session $session)
    {
        $domain = new BadDomain();
        $form = $this->createFormBuilder($domain)
            ->add('name', TextType::class)
            ->getForm();

        if ($session->has('badDomainAddRequest')) {
            // If we have request in session, handle it
            $form->handleRequest($session->get('badDomainAddRequest'));
            $session->remove('badDomainAddRequest');
        } else {
            // Otherwise handle current request
            $form->handleRequest($request);

            // Validation
            if ($form->isSubmitted()) {
                if ($form->isValid()) {
                    $this->getRepository()->save($domain);
                    $this->addFlash('success', 'Domain added to list');
                } else {
                    $session->set('badDomainAddRequest', $request);
                }

                return $this->redirectToRoute('bad_domain_list');
            }
        }

        return $this->render('BadDomain/_form.html.twig', ['form' => $form->createView()]);
    }


    /**
     * List of stored bad domains
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function list()
    {
        $domains = $this->getRepository()->findAll();

        return $this->render('BadDomain/list.html.twig', ['domains' => $domains]);

    }

    /**
     * Remove bad domain from storage
     *
     * @ParamConverter("domain", class="App:BadDomain", converter="doctrine.orm")
     *
     * @param BadDomain $domain
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function remove(BadDomain $domain)
    {
        $this->getRepository()->remove($domain);

        return $this->redirectToRoute('bad_domain_list');
    }

    /**
     * Get repository for BadDomain entity
     *
     * @return BadDomainRepositoryInterface BadDomain repository
     */
    protected function getRepository(): BadDomainRepositoryInterface
    {
        return $this->getDoctrine()->getRepository(BadDomain::class);
    }
}