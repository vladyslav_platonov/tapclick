<?php

namespace App\Service;

use App\Entity\Click;
use App\Repository\ClickRepositoryInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;


/**
 * Creates click with parameters from storage or new one
 */
class ClickCreator implements ClickCreatorInterface
{
    /**
     * @var ClickRepositoryInterface
     */
    protected $repository;

    /**
     * @var BadDomainCheckerInterface
     */
    protected $badDomainChecker;

    /**
     * @var ValidatorInterface
     */
    protected $validator;

    /**
     * @param ClickRepositoryInterface $repository Repository for clicks
     * @param BadDomainCheckerInterface $badDomainChecker Checker to check whether domain is bad one
     * @param ValidatorInterface $validator Validator to validate entity data
     */
    public function __construct(
        ClickRepositoryInterface $repository,
        BadDomainCheckerInterface $badDomainChecker,
        ValidatorInterface $validator
    ) {
        $this->repository = $repository;
        $this->badDomainChecker = $badDomainChecker;
        $this->validator = $validator;
    }

    /**
     * Creates click with parameters from storage or new one
     *
     * @param string $referer Referer url
     * @param string $ip IP address
     * @param string $userAgent User agent
     * @param string $param1 First parameter
     * @param string $param2 Second parameter
     *
     * @return Click Click object
     *
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function createClick(string $referer, string $ip, string $userAgent, string $param1, string $param2): Click
    {
        $click = $this->repository->findOneByData($referer, $ip, $userAgent, $param1);

        if (!$click) {
            $click = new Click($referer, $ip, $userAgent, $param1, $param2);
        }

        $click->setBadDomainChecker($this->badDomainChecker);
        $click->setValidator($this->validator);

        return $click;
    }
}