<?php

namespace App\Service;


/**
 * Creates click with parameters from storage or new one
 */
interface ClickCreatorInterface
{
    /**
     * Creates click with parameters from storage or new one
     *
     * @param string $referer Referer url
     * @param string $ip IP address
     * @param string $userAgent User agent
     * @param string $param1 First parameter
     * @param string $param2 Second parameter
     *
     * @return \App\Entity\Click Click object
     */
    public function createClick(string $referer, string $ip, string $userAgent, string $param1, string $param2): \App\Entity\Click;
}