<?php

namespace App\Service;

use App\Converter\UrlToDomainConverterInterface;
use App\Repository\BadDomainRepositoryInterface;


/**
 * Checks domain name of given url if domain name is bad one
 */
class BadDomainChecker implements BadDomainCheckerInterface
{
    /**
     * @var BadDomainRepositoryInterface Repository for bad domain entity
     */
    protected $domainRepository;

    /**
     * @var UrlToDomainConverterInterface Converter to convert domain name from given url
     */
    protected $urlToDomain;

    /**
     * @param BadDomainRepositoryInterface $domainRepository Repository for bad domain entity
     * @param UrlToDomainConverterInterface $urlToDomain Converter to convert domain name from given url
     */
    public function __construct(BadDomainRepositoryInterface $domainRepository, UrlToDomainConverterInterface $urlToDomain)
    {
        $this->domainRepository = $domainRepository;
        $this->urlToDomain = $urlToDomain;
    }

    /**
     * Check if domain from url is bad one
     *
     * @param string $url Url to check
     *
     * @return bool Whether domain name of given url is bad one
     */
    public function isBadUrlDomain(string $url): bool
    {
        $domainName = $this->urlToDomain->apply($url);

        $domain = $this->domainRepository->findOneByName($domainName);

        if ($domain) {
            return true;
        }

        return false;
    }

}