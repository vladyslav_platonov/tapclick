<?php

namespace App\Service;

/**
 * Checks domain name of given url if domain name is bad one
 */
interface BadDomainCheckerInterface
{
    /**
     * Check if domain from url is bad one
     *
     * @param string $url Url to check
     *
     * @return bool Whether domain name of given url is bad one
     */
    public function isBadUrlDomain(string $url): bool;
}