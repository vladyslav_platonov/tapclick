<?php

namespace App\Repository;


/**
 * Repository for Click entity
 */
interface ClickRepositoryInterface
{
    /**
     * Find all clicks from storage
     *
     * @param int $maxResults Limit for results, by default equals 1000
     *
     * @return \App\Entity\Click[] Collection of click objects
     */
    public function findAll($maxResults = 1000): array;

    /**
     * Find click by data from storage
     *
     * @param string $referer Referer of request
     * @param string $ip User IP address
     * @param string $userAgent User agent
     * @param string $param1 First parameter
     *
     * @return \App\Entity\Click|null Click object or null
     *
     * @throws \Doctrine\ORM\NonUniqueResultException Should not thrown, because we are searching by unique data
     */
    public function findOneByData(string $referer, string $ip, string $userAgent, string $param1);

    /**
     * Save click to storage
     *
     * @param \App\Entity\Click $click Click object
     *
     * @return bool Whether entity manager has current object
     */
    public function save(\App\Entity\Click $click): bool;
}