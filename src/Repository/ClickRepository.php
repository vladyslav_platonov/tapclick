<?php

namespace App\Repository;

use App\Entity\Click;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\ORMException;
use Symfony\Bridge\Doctrine\RegistryInterface;


/**
 * Repository for Click entity
 */
class ClickRepository extends ServiceEntityRepository implements ClickRepositoryInterface
{
    /**
     * @param RegistryInterface $registry Registry manager
     */
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Click::class);
    }

    /**
     * Find all clicks from storage
     *
     * @param int $maxResults Limit for results, by default equals 1000
     *
     * @return Click[] Collection of click objects
     */
    public function findAll($maxResults = 1000): array
    {
        $qb = $this->createQueryBuilder('c');

        $qb->select('c');

        $qb->setMaxResults($maxResults);

        return $qb->getQuery()->getResult();
    }

    /**
     * Find click by data from storage
     *
     * @param string $referer Referer of request
     * @param string $ip User IP address
     * @param string $userAgent User agent
     * @param string $param1 First parameter
     *
     * @return Click|null Click object or null
     *
     * @throws \Doctrine\ORM\NonUniqueResultException Should not thrown, because we are searching by unique data
     */
    public function findOneByData(string $referer, string $ip, string $userAgent, string $param1)
    {
        $qb = $this->createQueryBuilder('c');

        $qb->select('c');

        $qb->andWhere('c.referer = :referer')->setParameter('referer', $referer);
        $qb->andWhere('c.ip = :ip')->setParameter('ip', $ip);
        $qb->andWhere('c.userAgent = :userAgent')->setParameter('userAgent', $userAgent);
        $qb->andWhere('c.param1 = :param1')->setParameter('param1', $param1);

        $click = $qb->getQuery()->getOneOrNullResult();

        return $click;
    }

    /**
     * Save click to storage
     *
     * @param Click $click Click object
     *
     * @return bool Whether entity manager has current object
     *
     * @throws ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function save(Click $click): bool
    {
        $this->getEntityManager()->persist($click);
        $this->getEntityManager()->flush();

        return $this->getEntityManager()->contains($click);
    }
}