<?php

namespace App\Repository;

use App\Converter\UrlToDomainConverterInterface;
use App\Entity\BadDomain;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;


/**
 * Repository for BadDomain entity
 */
class BadDomainRepository extends ServiceEntityRepository implements BadDomainRepositoryInterface
{
    /**
     * @var UrlToDomainConverterInterface Converter for converting url to domain name
     */
    protected $urlToDomain;

    /**
     * @param RegistryInterface $registry Registry manager
     * @param UrlToDomainConverterInterface $urlToDomain Converter for converting url to domain name
     */
    public function __construct(RegistryInterface $registry, UrlToDomainConverterInterface $urlToDomain)
    {
        parent::__construct($registry, BadDomain::class);

        $this->urlToDomain = $urlToDomain;
    }

    /**
     * Find all bad domains from storage
     *
     * @param int $maxResults Limit for results, by default equals 1000
     *
     * @return BadDomain[] Collection of bad domain objects
     */
    public function findAll($maxResults = 1000): array
    {
        $qb = $this->createQueryBuilder('d');

        $qb->select('d');

        $qb->setMaxResults($maxResults);

        return $qb->getQuery()->getResult();
    }

    /**
     * Find domain by domain name
     *
     * @param string $name Domain name
     *
     * @return BadDomain|null BadDomain object or null
     *
     * @throws \Doctrine\ORM\NonUniqueResultException Should not thrown, because we are searching by unique data
     */
    public function findOneByName(string $name)
    {
        $qb = $this->createQueryBuilder('d');

        $qb->select('d');

        $qb->andWhere('d.name = :name')->setParameter('name', $name);

        $click = $qb->getQuery()->getOneOrNullResult();

        return $click;
    }

    /**
     * Save bad domain to storage
     *
     * @param BadDomain $domain Bad domain object
     *
     * @return bool Whether entity manager has current object
     *
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function save(BadDomain $domain): bool
    {
        $this->getEntityManager()->persist($domain);
        $this->getEntityManager()->flush();

        return $this->getEntityManager()->contains($domain);
    }

    /**
     * Remove bad domain from storage
     *
     * @param BadDomain $domain Bad domain object
     *
     * @return bool Whether entity manager doesn't have current object
     *
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function remove(BadDomain $domain): bool
    {
        $this->getEntityManager()->remove($domain);
        $this->getEntityManager()->flush();

        return !$this->getEntityManager()->contains($domain);
    }
}