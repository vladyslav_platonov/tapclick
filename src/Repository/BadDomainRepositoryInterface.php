<?php

namespace App\Repository;


/**
 * Repository for BadDomain entity
 */
interface BadDomainRepositoryInterface
{
    /**
     * Find all bad domains from storage
     *
     * @param int $maxResults Limit for results, by default equals 1000
     *
     * @return \App\Entity\BadDomain[] Collection of bad domain objects
     */
    public function findAll($maxResults = 1000): array;

    /**
     * Find domain by domain name
     *
     * @param string $name Domain name
     *
     * @return \App\Entity\BadDomain|null BadDomain object or null
     */
    public function findOneByName(string $name);

    /**
     * Save bad domain to storage
     *
     * @param \App\Entity\BadDomain $domain Bad domain object
     *
     * @return bool Whether entity manager has current object
     */
    public function save(\App\Entity\BadDomain $domain): bool;

    /**
     * Remove bad domain from storage
     *
     * @param \App\Entity\BadDomain $domain Bad domain object
     *
     * @return bool Whether entity manager doesn't have current object
     */
    public function remove(\App\Entity\BadDomain $domain): bool;
}