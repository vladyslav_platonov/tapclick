<?php

namespace App\Request\ParamConverter;

use App\Service\ClickCreatorInterface;
use Doctrine\Common\Persistence\ManagerRegistry;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Request\ParamConverter\ParamConverterInterface;
use Symfony\Component\HttpFoundation\Request;


/**
 * Converts parameters from request to Click class
 */
class ClickParamConverter implements ParamConverterInterface
{
    const CLASS_NAME = 'App\Entity\Click';

    /**
     * @var ManagerRegistry $registry Manager registry
     */
    private $registry;

    /**
     * @var ClickCreatorInterface Click creator that knows how to create Click object
     */
    private $clickCreator;

    /**
     * @param ManagerRegistry $registry Registry manager
     * @param ClickCreatorInterface $clickCreator Click creator that knows how to create Click object
     */
    public function __construct(ManagerRegistry $registry = null, ClickCreatorInterface $clickCreator)
    {
        $this->registry = $registry;
        $this->clickCreator = $clickCreator;
    }

    /**
     * Stores the object in the request.
     *
     * @param Request $request Request with needed parameters
     * @param ParamConverter $configuration Contains the name, class and options of the object
     *
     * @return void Doesn't send result, object can be found in request attributes
     *
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function apply(Request $request, ParamConverter $configuration)
    {
        // Get parameters from request
        $param1 = $request->query->get('param1');
        $param2 = $request->query->get('param2');
        $referer = $request->server->get('HTTP_REFERER');
        $userAgent = $request->server->get('HTTP_USER_AGENT');
        $ip = $request->server->get('REMOTE_ADDR');

        // Check if required parameters present
        if ($param1 === null || $userAgent === null || $ip === null || $referer === null) {
            throw new \InvalidArgumentException('Required parameters are missing');
        }

        // Create click object with given parameters
        $click = $this->clickCreator->createClick($referer, $ip, $userAgent, $param1, $param2);

        // Map click object to request attributes
        $request->attributes->set($configuration->getName(), $click);
    }

    /**
     * Checks if the class is supported.
     *
     * @param ParamConverter $configuration Configuration for converter
     *
     * @return bool True if the class is supported, else false
     */
    public function supports(ParamConverter $configuration)
    {
        // If there is no entity manager, than we have just Doctrine DBAL without ORM, so
        // we can not handle current request
        if (!($this->registry !== null && count($this->registry->getManagers()))) {
            return false;
        }

        // CHeck if we have class in annotation
        if ($configuration->getClass() === null) {
            return false;
        }

        // Get entity manager for needed class
        $em = $this->registry->getManagerForClass($configuration->getClass());

        // Check if class name from configuration equals our needed class
        if ($em->getClassMetadata($configuration->getClass())->getName() !== self::CLASS_NAME) {
            return false;
        }

        return true;
    }
}