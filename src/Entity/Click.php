<?php

namespace App\Entity;

use App\Service\BadDomainCheckerInterface;
use App\Traits\TimestampTrait;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Exception\ValidatorException;
use Symfony\Component\Validator\Validator\ValidatorInterface;


/**
 * Click entity
 *
 * @UniqueEntity(
 *     fields={"referer", "userAgent", "ip", "param1"},
 *     errorPath="referer",
 *     message="Click data already exist"
 * )
 */
class Click
{
    use TimestampTrait;

    /**
     * @var \Ramsey\Uuid\Uuid
     */
    private $id;

    /**
     * @var string
     * @Assert\NotBlank
     * @Assert\Type("string")
     */
    private $userAgent;

    /**
     * @var string
     * @Assert\NotBlank
     * @Assert\Ip
     */
    private $ip;

    /**
     * @var string
     * @Assert\NotBlank
     * @Assert\Url
     */
    private $referer;

    /**
     * @var string
     * @Assert\NotBlank
     * @Assert\Type("string")
     */
    private $param1;

    /**
     * @var string
     * @Assert\NotBlank
     * @Assert\Type("string")
     */
    private $param2;

    /**
     * @var int
     * @Assert\Type("integer")
     */
    private $errorCount = 0;

    /**
     * @var bool
     * @Assert\Type("bool")
     */
    private $badDomain = false;

    /**
     * @var BadDomainCheckerInterface
     */
    private $badDomainChecker;

    /**
     * @var ValidatorInterface
     */
    private $validator;

    /**
     * Click constructor.
     *
     * @param string $referer Referer url
     * @param string $ip IP address
     * @param string $userAgent User agent
     * @param string $param1 First parameter
     * @param string $param2 Second parameter
     */
    public function __construct(string $referer, string $ip, string $userAgent, string $param1, string $param2)
    {
        $this->referer = $referer;
        $this->ip = $ip;
        $this->userAgent = $userAgent;
        $this->param1 = $param1;
        $this->param2 = $param2;
    }

    /**
     * @return \Ramsey\Uuid\Uuid UUID of current click
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string User agent
     */
    public function getUserAgent()
    {
        return $this->userAgent;
    }

    /**
     * @return string User IP address
     */
    public function getIp()
    {
        return $this->ip;
    }

    /**
     * @return string Referer of click
     */
    public function getReferer()
    {
        return $this->referer;
    }

    /**
     * @return string First parameter
     */
    public function getParam1()
    {
        return $this->param1;
    }

    /**
     * @return string Second parameter
     */
    public function getParam2()
    {
        return $this->param2;
    }

    /**
     * @return int Error count for click
     */
    public function getErrorCount(): int
    {
        return $this->errorCount;
    }

    /**
     * @return bool Whether domain of referer is bad one
     */
    public function isBadDomain(): bool
    {
        return $this->badDomain;
    }

    /**
     * Handle click and check if it's unique
     *
     * @return bool If click is unique
     *
     * @throws ValidatorException
     */
    public function handleClick(): bool
    {
        // Validate object data
        $this->validate();

        // Check if domain is bad one
        $this->checkBadDomain();

        // If it's not new record or domain is bad one, click is failed
        if ($this->getId() || $this->isBadDomain()) {
            $this->handleBadClick();
            return false;
        }

        return true;
    }

    /**
     * @param BadDomainCheckerInterface $checker Checker to check whether domain from referer is bad one
     *
     * @return void
     */
    public function setBadDomainChecker(BadDomainCheckerInterface $checker): void
    {
        $this->badDomainChecker = $checker;
    }

    /**
     * @param ValidatorInterface $validator Validator to validate data of entity
     *
     * @return void
     */
    public function setValidator(ValidatorInterface $validator): void
    {
        $this->validator = $validator;
    }

    /**
     * Check if domain is bad one
     *
     * @return void
     */
    protected function checkBadDomain(): void
    {
        if ($this->badDomainChecker instanceof BadDomainCheckerInterface) {
            $this->badDomain = $this->badDomainChecker->isBadUrlDomain($this->referer);
        }
    }

    /**
     * Data validation for current object
     * @return void
     * @throws ValidatorException
     */
    protected function validate(): void
    {
        if ($this->validator instanceof ValidatorInterface) {
            $errors = $this->validator->validate($this);

            if (count($errors) > 0) {
                throw new ValidatorException(sprintf('Data is not valid, errors: %s', print_r($errors, true)));
            }
        }
    }

    /**
     * If click is not unique or domain is bad one, handle error click
     *
     * @return void
     */
    protected function handleBadClick(): void
    {
        $this->errorCount++;
    }
}