<?php

namespace App\Entity;

use App\Traits\TimestampTrait;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;


/**
 * Bad domain entity
 *
 * @UniqueEntity("name")
 */
class BadDomain
{
    use TimestampTrait;

    /**
     * @var \Ramsey\Uuid\Uuid
     */
    private $id;

    /**
     * @var string
     * @Assert\NotBlank
     * @Assert\Regex("/^[\w\d-\.]+\.[\w]{2,5}/")
     */
    private $name;

    /**
     * @return \Ramsey\Uuid\Uuid
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     *
     * @return $this
     */
    public function setName(string $name)
    {
        $this->name = $name;

        return $this;
    }
}