<?php

namespace App\Traits;


/**
 * Trait to give entities created and updated dates behavior
 */
trait TimestampTrait
{
    /**
     * @var \DateTime Date object
     */
    protected $createdAt;

    /**
     * @var \DateTime Date object
     */
    protected $updatedAt;

    /**
     * Returns created date
     *
     * @return \DateTime Date object
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Returns updated date
     *
     * @return \DateTime Date object
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set new created date
     *
     * @param \DateTime $createdAt Date object
     *
     * @return $this
     */
    public function setCreatedAt(\DateTime $createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Set new updated date
     *
     * @param \DateTime $updatedAt Date object
     *
     * @return $this
     */
    public function setUpdatedAt(\DateTime $updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Updates created and updated dates.
     */
    public function updateTimestamps()
    {
        $dateTime = new \DateTime();

        if ($this->createdAt === null) {
            $this->createdAt = $dateTime;
        }

        $this->updatedAt = $dateTime;
    }
}